<p>
    <h1>How to run</h1>
    copy .env.example to .env

    docker-compose up -d
    composer install
    php artisan migrate
    php artisan db:seed
    php artisan serve
</p>
