<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Notebook;
use Illuminate\Http\Request;

class NotebookController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Notebook::all();
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $notebook = Notebook::create($request->all());
        return response()->json($notebook, 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $notebook = Notebook::find($id);
        if($notebook === null) {
            return response()->json(['message' => 'Notebook not found'], 404);
        }

        return Notebook::find($id);
    }

    public function update(Request $request, int $id)
    {
        $notebook = Notebook::find($id);
        if($notebook === null) {
            return response()->json(['message' => 'Notebook not found'], 404);
        }

        $notebook->update($request->all());
        return response()->json($notebook, 200);
    }

    public function destroy(Notebook $notebook)
    {
        $notebook->delete();
        return response()->json(null, 204);
    }
}
